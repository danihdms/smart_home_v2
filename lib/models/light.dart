import 'exports.dart';

class Light extends Device {
  Light(light) : super(light);

  static void setIntensity(Device device, int intensity) {
    if (intensity == 0) {
      device.intensity = 0;
      setMode(device, Mode.off);
    } else if (intensity == 100) {
      setMode(device, Mode.on);
      device.intensity = 100;
    } else {
      setMode(device, Mode.on);
      device.intensity = intensity + 1;
    }
  }

  static void setMode(Device device, Enum mode) {
    device.mode = mode;
  }

  static String printAtt(Device dev) {
    String strMode = (dev.mode == Mode.on) ? 'ON' : 'OFF';
    return '$strMode - Intensity : ${dev.intensity}';
  }
}
