import 'exports.dart';

class Heater extends Device {
  Heater(heater) : super(heater);

  static void setTemperature(Device device, int temperature) {
    if (temperature == 5) {
      device.temperature = 5;
      setMode(device, Mode.off);
    } else if (device.temperature != null && temperature <= 27) {
      device.temperature = temperature + 1;
    } else {
      device.temperature = 28;
    }
    if (temperature > 5 && device.mode == Mode.on) {
      setMode(device, Mode.on);
    } else {
      setMode(device, Mode.off);
    }
  }

  static void setMode(Device device, Enum mode) {
    device.mode = mode;
  }

  static String printAtt(Device device) {
    String strMode = (device.mode == Mode.on) ? 'ON' : 'OFF';
    if (device.mode == Mode.on) {
      return '$strMode at ${device.temperature}°C';
    } else {
      return strMode;
    }
  }
}
