import 'exports.dart';

class Device {
  late int _id;
  late String _deviceName;
  late String _productType;

  late int? _intensity;
  late Enum? _mode;

  late int? _position;

  late int? _temperature;

  Device(device) {
    _id = device['id'];
    _deviceName = device['deviceName'];
    _productType = device['productType'];
    _intensity = device['intensity'];
    if (device['mode'] == 'ON') {
      _mode = Mode.on;
    } else {
      _mode = Mode.off;
    }
    _position = device['position'];
    _temperature = device['temperature'];
  }

  int get id => _id;
  String get deviceName => _deviceName;
  String get productType => _productType;
  int? get intensity => _intensity;
  int? get position => _position;
  Enum? get mode => _mode;
  int? get temperature => _temperature;

  set mode(value) => _mode = value;
  set intensity(value) => _intensity = value;
  set position(value) => _position = value;
  set temperature(value) => _temperature = value;

  @override
  String toString() {
    if (intensity != null) {
      return Light.printAtt(this);
    } else if (position != null) {
      return RollerShutter.printAtt(this);
    } else {
      return Heater.printAtt(this);
    }
  }
}

enum Mode { on, off }
