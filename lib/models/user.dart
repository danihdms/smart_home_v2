import 'exports.dart';

class User {
  late String _firstName;
  late String _lastName;
  late Address _address;
  late int _birthDate;

  User(user) {
    _firstName = user['firstName'];
    _lastName = user['lastName'];
    _address = Address(user['address']);
    _birthDate = user['birthDate'];
  }

  String get firstName => _firstName;
  String get lastName => _lastName;
  Address get address => _address;
  int get birthDate => _birthDate;
}
