class Address {
  late String _city;
  late int _postalCode;
  late String _street;
  late String _streetCode;
  late String _country;

  Address(address) {
    _city = address['city'];
    _postalCode = address['postalCode'];
    _street = address['street'];
    _streetCode = address['streetCode'];
    _country = address['country'];
  }

  String get city => _city;
  int get postalCode => _postalCode;
  String get street => _street;
  String get streetCode => _streetCode;
  String get country => _country;
}
