import 'exports.dart';

class RollerShutter extends Device {
  RollerShutter(rollerShutter) : super(rollerShutter);

  static void setPosition(Device device, int position) {
    if (position >= 100) {
      device.position = 100;
    } else {
      if (position == 0) {
        device.position = 0;
      } else {
        device.position = position + 1;
      }
    }
  }

  static String printAtt(Device device) {
    if (device.position == 0) {
      return 'Closed';
    } else if (device.position == 100) {
      return 'Opened';
    } else {
      return 'Opened at ${device.position}%';
    }
  }
}
