import 'exports.dart';

class ItemModel {
  late List<Device> _devices;
  late User _user;
  late final List<String> _devTypes = getDeviceTypes(_devices);
  late final List<Device> _lights = getLights(_devices);
  late final List<Device> _heaters = getHeaters(_devices);
  late final List<Device> _rollerShutters = getRollerShutters(_devices);

  ItemModel.fromJson(Map<String, dynamic> parsedJson) {
    List<Device> tmp = [];
    for (int i = 0; i < parsedJson['devices'].length; i++) {
      Device device = Device(parsedJson['devices'][i]);
      tmp.add(device);
    }
    _devices = tmp;
    _user = User(parsedJson['user']);
  }

  List<Device> get devices => _devices;
  User get user => _user;
  List<String> get devTypes => _devTypes;
  List<Device> get lights => _lights;
  List<Device> get heaters => _heaters;
  List<Device> get rollerShutters => _rollerShutters;

  List<String> getDeviceTypes(List<Device> devices) {
    List<String> types = [];
    for (int i = 0; i < devices.length; i++) {
      if (!types.contains(devices[i].productType)) {
        types.add(devices[i].productType);
      }
    }
    return types;
  }

  List<Device> getLights(List<Device> devices) {
    List<Device> lights = [];
    for (int i = 0; i < devices.length; i++) {
      if (devices[i].productType == 'Light') {
        lights.add(devices[i]);
      }
    }
    return lights;
  }

  List<Device> getHeaters(List<Device> devices) {
    List<Device> heaters = [];
    for (int i = 0; i < devices.length; i++) {
      if (devices[i].productType == 'Heater') {
        heaters.add(devices[i]);
      }
    }
    return heaters;
  }

  List<Device> getRollerShutters(List<Device> devices) {
    List<Device> rollerShutters = [];
    for (int i = 0; i < devices.length; i++) {
      if (devices[i].productType == 'RollerShutter') {
        rollerShutters.add(devices[i]);
      }
    }
    return rollerShutters;
  }
}
