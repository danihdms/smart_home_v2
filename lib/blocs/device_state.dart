import 'package:equatable/equatable.dart';
import 'package:smart_home/models/item_model.dart';

abstract class DeviceState extends Equatable {}

class DeviceInitialState extends DeviceState {
  @override
  List<Object?> get props => [];
}

class DeviceLoadingState extends DeviceState {
  @override
  List<Object?> get props => [];
}

class DeviceLoadedState extends DeviceState {
  ItemModel model;

  DeviceLoadedState({required this.model});

  @override
  List<Object?> get props => [model];
}

class DeviceErrorState extends DeviceState {
  String message;

  DeviceErrorState({required this.message});

  @override
  List<Object?> get props => [message];
}
