import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_home/blocs/device_event.dart';
import 'package:smart_home/blocs/device_state.dart';
import 'package:smart_home/resources/device_repository.dart';

class DeviceBloc extends Bloc<DeviceEvent, DeviceState> {
  DeviceRepository repository;

  DeviceBloc(this.repository) : super(DeviceInitialState()) {
    on<DeviceEvent>(((event, emit) async {
      emit(DeviceInitialState());
      try {
        final model = await repository.fetchDevices();
        emit(DeviceLoadedState(model: model));
      } catch (error) {
        print(error.toString());
        emit(DeviceErrorState(message: error.toString()));
      }
    }));
  }
}

final bloc = DeviceBloc(DeviceRepositoryImpl());
