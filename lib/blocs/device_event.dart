import 'package:equatable/equatable.dart';

abstract class DeviceEvent extends Equatable {}

class FetchDevicesEvent extends DeviceEvent {
  @override
  List<Object> get props => [];
}
