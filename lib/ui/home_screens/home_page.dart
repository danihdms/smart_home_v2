import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_home/blocs/device_bloc.dart';
import 'package:smart_home/blocs/device_event.dart';
import 'package:smart_home/ui/device_screens/device_page.dart';
import 'package:smart_home/ui/home_screens/header_home_page.dart';
import '../../blocs/device_state.dart';
import '../../models/exports.dart';
import '../constants.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  late DeviceBloc deviceBloc;

  @override
  void initState() {
    super.initState();
    deviceBloc = BlocProvider.of<DeviceBloc>(context);
    deviceBloc.add(FetchDevicesEvent());
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Builder(
        builder: (context) {
          return Material(
            child: Scaffold(
              appBar: AppBar(
                elevation: 0,
                backgroundColor: kBrown,
                leading:
                    Image.asset('assets/AppIcon.appiconset/120-removebg.png'),
              ),
              body: Container(
                child: BlocBuilder<DeviceBloc, DeviceState>(
                  builder: (context, state) {
                    if (state is DeviceInitialState) {
                      return buildLoading();
                    } else if (state is DeviceLoadingState) {
                      return buildLoading();
                    } else if (state is DeviceLoadedState) {
                      return buildList(state.model);
                    } else if (state is DeviceErrorState) {
                      return buildError(state.message);
                    } else {
                      throw NullThrownError();
                    }
                  },
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget buildLoading() {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildError(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(kDefaultPadding),
        child: Text(
          message,
          style: const TextStyle(color: kBrown),
        ),
      ),
    );
  }

  Widget buildList(ItemModel model) {
    int subDevCount;
    List<Device> tmp;
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          HeaderHomePage(
            size: size,
            user: model.user,
          ),
          ListView.builder(
            physics: const ClampingScrollPhysics(),
            shrinkWrap: true,
            itemCount: model.devTypes.length,
            itemBuilder: (BuildContext context, int index) {
              if (model.devTypes[index] == 'Light') {
                tmp = model.lights;
                subDevCount = model.lights.length;
              } else if (model.devTypes[index] == 'Heater') {
                tmp = model.heaters;
                subDevCount = model.heaters.length;
              } else {
                tmp = model.rollerShutters;
                subDevCount = model.rollerShutters.length;
              }
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin:
                        const EdgeInsets.symmetric(horizontal: kDefaultPadding),
                    padding: const EdgeInsets.symmetric(
                        horizontal: kDefaultPadding, vertical: kDefaultPadding),
                    child: Text(
                      model.devTypes[index],
                      style: TextStyle(
                        shadows: <Shadow>[
                          Shadow(
                            offset: const Offset(10.0, 10.0),
                            blurRadius: 3.0,
                            color: kBrown.withOpacity(0.23),
                          ),
                        ],
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: kBrown,
                      ),
                    ),
                  ),
                  ListView.builder(
                    itemCount: subDevCount,
                    physics: const ClampingScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int i) {
                      return Container(
                        padding: const EdgeInsets.only(
                          left: kDefaultPadding * 2,
                          bottom: kDefaultPadding,
                          right: kDefaultPadding * 2,
                        ),
                        child: ElevatedButton(
                          onPressed: () {
                            buildDevicePage(model, index, i);
                          },
                          style: ElevatedButton.styleFrom(
                              minimumSize: const Size.fromHeight(100),
                              backgroundColor: kWhite),
                          child: Row(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    tmp[i].deviceName,
                                    style: const TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500,
                                      color: kLightSteelBlue,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                  Text(
                                    (tmp[i].toString()),
                                    style:
                                        const TextStyle(color: kLightSteelBlue),
                                    textAlign: TextAlign.left,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ],
              );
            },
          ),
        ],
      ),
    );
  }

  Future<dynamic> buildDevicePage(ItemModel model, int index, int i) {
    List<Device> tmp;
    if (index == 0) {
      tmp = model.lights;
    } else if (index == 1) {
      tmp = model.rollerShutters;
    } else {
      tmp = model.heaters;
    }
    return Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => DevicePage(
          device: tmp[i],
        ),
      ),
    ).then((value) => setState(
          () {},
        ));
  }
}
