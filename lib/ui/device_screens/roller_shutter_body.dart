import 'package:flutter/material.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';
import '../../models/exports.dart';
import '../constants.dart';
import 'header_device.dart';

class RollerShutterBody extends StatefulWidget {
  final Device device;

  const RollerShutterBody({super.key, required this.device});

  @override
  // ignore: no_logic_in_create_state
  State<StatefulWidget> createState() => RollerShutterBodyState(device: device);
}

class RollerShutterBodyState extends State<RollerShutterBody> {
  Device device;
  RollerShutterBodyState({required this.device});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    String opened;

    if (device.position == 0) {
      opened = 'Closed';
    } else {
      opened = 'Opened at';
    }
    double d = 0;
    return Column(
      children: [
        HeaderDevice(size: size, device: device),
        Column(
          children: <Widget>[
            Text(
              device.deviceName,
              style: const TextStyle(
                fontSize: 45,
                fontWeight: FontWeight.bold,
                color: kBrown,
              ),
            ),
            const SizedBox(
              height: kDefaultPadding * 3,
            ),
          ],
        ),
        SleekCircularSlider(
          initialValue: device.position?.toDouble() ?? 0,
          appearance: CircularSliderAppearance(
              size: MediaQuery.of(context).size.width * 0.35,
              infoProperties: InfoProperties(
                  bottomLabelText: opened,
                  bottomLabelStyle: const TextStyle(color: kBrown)),
              customColors: CustomSliderColors(
                shadowColor: kBrown,
                shadowMaxOpacity: 0.2,
                progressBarColors: [kBrown, kWhite],
                trackColor: kLightSteelBlue,
              )),
          onChange: (value) => RollerShutter.setPosition(device, value.toInt()),
        ),
      ],
    );
  }
}
