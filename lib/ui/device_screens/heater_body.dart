import 'package:flutter/material.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';
import '../../models/exports.dart';
import '../constants.dart';
import 'header_device.dart';

class HeaterBody extends StatefulWidget {
  final Device device;

  const HeaterBody({super.key, required this.device});

  @override
  // ignore: no_logic_in_create_state
  State<StatefulWidget> createState() => HeaterBodyState(device: device);
}

class HeaterBodyState extends State<HeaterBody> {
  final Device device;

  HeaterBodyState({required this.device});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        HeaderDevice(size: size, device: device),
        Column(
          children: <Widget>[
            Text(
              device.deviceName,
              style: const TextStyle(
                fontSize: 45,
                fontWeight: FontWeight.bold,
                color: kBrown,
              ),
            ),
            const SizedBox(
              height: kDefaultPadding * 3,
            ),
          ],
        ),
        SleekCircularSlider(
          initialValue: device.temperature?.toDouble() ?? 0,
          min: 5,
          max: 28,
          appearance: CircularSliderAppearance(
              size: MediaQuery.of(context).size.width * 0.35,
              infoProperties: InfoProperties(
                  modifier: (percentage) {
                    final roundedValue = percentage.ceil().toInt().toString();
                    return '$roundedValue °C';
                  },
                  bottomLabelText: 'Temperature',
                  bottomLabelStyle: const TextStyle(color: kBrown)),
              customColors: CustomSliderColors(
                shadowColor: kBrown,
                shadowMaxOpacity: 0.2,
                progressBarColors: [kBrown, kWhite],
                trackColor: kLightSteelBlue,
              )),
          onChange: (value) => Heater.setTemperature(device, value.toInt()),
        ),
        LiteRollingSwitch(
          value: (device.mode == Mode.on),
          textOn: 'ON',
          textOff: 'OFF',
          colorOn: kBrown,
          colorOff: kBrown.withOpacity(0.23),
          iconOn: Icons.done,
          iconOff: Icons.blur_off,
          onSwipe: () {},
          onChanged: (mode) {
            setState(() {
              if (mode) {
                device.mode = Mode.on;
              } else {
                device.mode = Mode.off;
              }
            });
          },
          onDoubleTap: () {},
          onTap: () {},
        )
      ],
    );
  }
}
