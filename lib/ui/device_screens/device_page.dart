import 'package:flutter/material.dart';
import 'package:smart_home/ui/device_screens/light_body.dart';
import 'package:smart_home/ui/device_screens/roller_shutter_body.dart';
import '../../models/exports.dart';
import '../constants.dart';
import 'heater_body.dart';

// ignore: must_be_immutable
class DevicePage extends StatelessWidget {
  Device device;

  DevicePage({
    super.key,
    required this.device,
  });

  @override
  Widget build(BuildContext context) {
    Widget tmp = LightBody(device: device);
    if (device.intensity != null) {
      tmp = LightBody(device: device);
    } else if (device.position != null) {
      tmp = RollerShutterBody(device: device);
    } else {
      tmp = HeaterBody(device: device);
    }
    return Scaffold(
      appBar: buildAppBar(context),
      body: tmp,
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: kBrown,
      leading: IconButton(
        icon: Image.asset('assets/back_arrow.png'),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}
