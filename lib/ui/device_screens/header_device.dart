import 'package:flutter/material.dart';
import '../../models/exports.dart';
import '../constants.dart';

class HeaderDevice extends StatelessWidget {
  const HeaderDevice({
    Key? key,
    required this.size,
    required this.device,
  }) : super(key: key);

  final Size size;
  final Device device;

  @override
  Widget build(BuildContext context) {
    String img;
    if (device.intensity != null) {
      if (device.mode == Mode.on) {
        img = 'assets/DeviceLightOnIcon.png';
      } else {
        img = 'assets/DeviceLightOffIcon.png';
      }
    } else if (device.temperature != null) {
      if (device.mode == Mode.on) {
        img = 'assets/DeviceHeaterOnIcon.png';
      } else {
        img = 'assets/DeviceHeaterOffIcon.png';
      }
    } else {
      img = 'assets/DeviceRollerShutterIcon.png';
    }
    return Container(
      margin: const EdgeInsets.only(bottom: kDefaultPadding * 2.5),
      height: size.height * 0.2,
      child: Stack(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(
              left: kDefaultPadding,
              bottom: 36 + kDefaultPadding,
            ),
            height: size.height * 0.2 - 27,
            decoration: BoxDecoration(
              color: kBrown,
              borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(36),
                bottomRight: Radius.circular(36),
              ),
              boxShadow: [
                BoxShadow(
                  offset: const Offset(0, 10),
                  blurRadius: 50,
                  color: kLightSteelBlue.withOpacity(0.23),
                ),
              ],
            ),
            child: Center(
              child: SizedBox(
                height: 130,
                child: Image.asset(img),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
