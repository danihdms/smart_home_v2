import 'dart:convert';

import '../models/exports.dart';
import 'package:http/http.dart' show Client;

abstract class DeviceRepository {
  Future<ItemModel> fetchDevices();
}

class DeviceRepositoryImpl implements DeviceRepository {
  Client client = Client();

  @override
  Future<ItemModel> fetchDevices() async {
    final response = await client
        .get(Uri.parse("http://storage42.com/modulotest/data.json"));
    if (response.statusCode == 200) {
      return ItemModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load devices');
    }
  }
}
