import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_home/blocs/device_bloc.dart';
import 'package:smart_home/resources/device_repository.dart';
import 'package:smart_home/ui/constants.dart';
import 'package:smart_home/ui/home_screens/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Smart Home',
      theme: ThemeData(
        scaffoldBackgroundColor: kWhite,
        primaryColor: kLightSteelBlue,
        textTheme: Theme.of(context).textTheme.apply(bodyColor: kDarkGray),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider(
          create: (context) => DeviceBloc(
                DeviceRepositoryImpl(),
              ),
          child: const HomePage()),
    );
  }
}
